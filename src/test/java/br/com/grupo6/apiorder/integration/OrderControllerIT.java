package br.com.grupo6.apiorder.integration;

import br.com.grupo6.apiorder.dao.OrderDAO;
import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.dto.PutOrderDTO;
import br.com.grupo6.apiorder.models.Order;
import br.com.grupo6.apiorder.util.OrderCreator;
import br.com.grupo6.apiorder.util.PostOrderRequestBody;
import br.com.grupo6.apiorder.util.PutOrderRequestBody;
import br.com.grupo6.apiorder.wrapper.PageableResponse;
import lombok.extern.log4j.Log4j2;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.springframework.boot.test.web.server.LocalServerPort;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.annotation.DirtiesContext;

import java.util.List;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureTestDatabase
@Log4j2
public class OrderControllerIT {
    @Autowired
    private TestRestTemplate testRestTemplate;
    @Autowired
    private OrderDAO orderDAO;
    @LocalServerPort
    private int port;

    @Test
    @DisplayName("Returns list of orders inside page object when successful")
    @DirtiesContext(classMode = DirtiesContext.ClassMode.BEFORE_EACH_TEST_METHOD)
    void returnsListOfOrdersInsidePageObjectWhenSuccesful() {
        Order savedOrder = orderDAO.save(OrderCreator.createOrder());

        PageableResponse<Order> orderPage = testRestTemplate.exchange("/orders", HttpMethod.GET,
                null, new ParameterizedTypeReference<PageableResponse<Order>>() {
        }).getBody();


        Assertions.assertThat(orderPage).isNotNull();
        Assertions.assertThat(orderPage.toList())
                .isNotEmpty()
                .hasSizeGreaterThanOrEqualTo(1);

        List<Order> listOrder = orderPage.toList();

        Order orderReturned = listOrder.get(listOrder.size() - 1);

        Assertions.assertThat(orderReturned.getUserId()).isEqualTo(savedOrder.getUserId());
        Assertions.assertThat(orderReturned.getOrderDate()).isEqualTo(savedOrder.getOrderDate());
        Assertions.assertThat(orderReturned.getDescription()).isEqualTo(savedOrder.getDescription());
        Assertions.assertThat(orderReturned.getStatus()).isEqualTo(savedOrder.getStatus());
        Assertions.assertThat(orderReturned.getValue()).isEqualTo(savedOrder.getValue());

    }


    @Test
    @DisplayName("Returns order found by id")
    void returnsOrderFoundById() {
        Order savedOrder = orderDAO.save(OrderCreator.createOrder());

        Order order = testRestTemplate.getForObject("/orders/{id}", Order.class, savedOrder.getId());

        Assertions.assertThat(order).isNotNull();
        Assertions.assertThat(order.getUserId()).isEqualTo(savedOrder.getUserId());
        Assertions.assertThat(order.getOrderDate()).isEqualTo(savedOrder.getOrderDate());
        Assertions.assertThat(order.getDescription()).isEqualTo(savedOrder.getDescription());
        Assertions.assertThat(order.getStatus()).isEqualTo(savedOrder.getStatus());
        Assertions.assertThat(order.getValue()).isEqualTo(savedOrder.getValue());
    }

    @Test
    @DisplayName("Save order when successful")
    void saveOrderWhenSuccessful() {
        PostOrderDTO postOrderRequestBody = PostOrderRequestBody.createPostOrderRequestBody();

        ResponseEntity<Order> orderResponseEntity = testRestTemplate.postForEntity("/orders", postOrderRequestBody, Order.class);

        Assertions.assertThat(orderResponseEntity).isNotNull();
        Assertions.assertThat(orderResponseEntity.getStatusCode()).isEqualTo(HttpStatus.CREATED);
        Assertions.assertThat(orderResponseEntity.getBody()).isNotNull();

        Order order = orderResponseEntity.getBody();
        Assertions.assertThat(order.getValue()).isEqualTo(postOrderRequestBody.getValue());
        Assertions.assertThat(order.getDescription()).isEqualTo(postOrderRequestBody.getDescription());
        Assertions.assertThat(order.getUserId()).isEqualTo(postOrderRequestBody.getUserId());
    }

    @Test
    @DisplayName("Update order when successful")
    void updateOrderWhenSuccessful() {
        Order savedOrder = orderDAO.save(OrderCreator.createOrder());

       PutOrderDTO putOrderDTO = PutOrderRequestBody.createPutOrderRequestBodyForUpdate();

       ResponseEntity<Void> orderResponseEntity = testRestTemplate.exchange("/orders/{id}", HttpMethod.PUT, new HttpEntity<>(putOrderDTO), Void.class, savedOrder.getId());


        Assertions.assertThat(orderResponseEntity).isNotNull();

        Assertions.assertThat(orderResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

    @Test
    @DisplayName("Delete order when successful")
    void deleteOrderWhenSuccessful() {
        Order savedOrder = orderDAO.save(OrderCreator.createOrder());

        ResponseEntity<Void> orderResponseEntity = testRestTemplate.exchange("/orders/{id}", HttpMethod.DELETE, null, Void.class, savedOrder.getId());

        Assertions.assertThat(orderResponseEntity).isNotNull();

        Assertions.assertThat(orderResponseEntity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

}
