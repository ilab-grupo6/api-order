package br.com.grupo6.apiorder.controllers;


import br.com.grupo6.apiorder.dao.OrderDAO;
import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.dto.PutOrderDTO;
import br.com.grupo6.apiorder.models.Order;
import br.com.grupo6.apiorder.services.IOrderService;
import br.com.grupo6.apiorder.util.OrderCreator;
import br.com.grupo6.apiorder.util.PostOrderRequestBody;
import br.com.grupo6.apiorder.util.PutOrderRequestBody;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.UUID;

@ExtendWith(SpringExtension.class)
@DisplayName("Tests for OrderController")
class OrderControllerTest {
    @InjectMocks
    private OrderController orderController;

    @Mock
    private IOrderService orderServiceMock;

    @BeforeEach
    void setUp() {
        PageImpl<Order> orderPage = new PageImpl<>(List.of(OrderCreator.createValidOrder()));
        BDDMockito.when(orderServiceMock.orderPage(ArgumentMatchers.any()))
                .thenReturn(orderPage);

        BDDMockito.when(orderServiceMock.findById(ArgumentMatchers.any(UUID.class)))
                .thenReturn(OrderCreator.createValidOrder());

        BDDMockito.when(orderServiceMock.save(ArgumentMatchers.any(PostOrderDTO.class)))
                .thenReturn(OrderCreator.createValidOrder());

        BDDMockito.when(orderServiceMock.update(ArgumentMatchers.any(UUID.class), ArgumentMatchers.any(PutOrderDTO.class)))
                .thenReturn(OrderCreator.createValidUpdatedOrder());

        BDDMockito.doNothing().when(orderServiceMock).delete(ArgumentMatchers.any(UUID.class));
    }

    @Test
    @DisplayName("Returns page of orders when successful")
    void returnsPageOfOrderswhenSuccessful() {
        Order order = OrderCreator.createValidOrder();

        Page<Order> orderPage = orderController.returnPage(null).getBody();

        Assertions.assertThat(orderPage).isNotNull();
        Assertions.assertThat(orderPage.toList())
                .isNotEmpty()
                .hasSize(1);

        Order orderReturned = orderPage.toList().get(0);

        Assertions.assertThat(orderReturned.getUserId()).isEqualTo(order.getUserId());
        Assertions.assertThat(orderReturned.getOrderDate()).isEqualTo(order.getOrderDate());
        Assertions.assertThat(orderReturned.getDescription()).isEqualTo(order.getDescription());
        Assertions.assertThat(orderReturned.getStatus()).isEqualTo(order.getStatus());
        Assertions.assertThat(orderReturned.getValue()).isEqualTo(order.getValue());
    }


    @Test
    @DisplayName("Returns order found by id")
    void returnsOrderFoundById() {
        Order order = OrderCreator.createValidOrder();

        Order returnedOrder = orderController.findById(order.getId()).getBody();

        Assertions.assertThat(returnedOrder).isNotNull();
        Assertions.assertThat(returnedOrder.getUserId()).isEqualTo(order.getUserId());
        Assertions.assertThat(returnedOrder.getOrderDate()).isEqualTo(order.getOrderDate());
        Assertions.assertThat(returnedOrder.getDescription()).isEqualTo(order.getDescription());
        Assertions.assertThat(returnedOrder.getStatus()).isEqualTo(order.getStatus());
        Assertions.assertThat(returnedOrder.getValue()).isEqualTo(order.getValue());

    }

    @Test
    @DisplayName("Save order when successful")
    void saveOrderWhenSuccessful() {
        Order order = orderController.save(PostOrderRequestBody.createPostOrderRequestBody()).getBody();

        Assertions.assertThat(order).isNotNull();
    }

    @Test
    @DisplayName("Update order when successful")
    void updateOrderWhenSuccessful() {
        Order order = OrderCreator.createValidUpdatedOrder();

        ResponseEntity<Void> entity = orderController.update(PutOrderRequestBody
                .createPutOrderRequestBodyForUpdate(),
                OrderCreator.createValidUpdatedOrder().getId());

        Assertions.assertThat(entity).isNotNull();
        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);

    }

    @Test
    @DisplayName("Delete order when successful")
    void deleteOrderWhenSuccessful() {

        Assertions.assertThatCode(() -> orderController.delete(UUID.randomUUID()))
                .doesNotThrowAnyException();

        ResponseEntity<Void> entity = orderController.delete(UUID.randomUUID());

        Assertions.assertThat(entity).isNotNull();

        Assertions.assertThat(entity.getStatusCode()).isEqualTo(HttpStatus.NO_CONTENT);
    }

}