package br.com.grupo6.apiorder.services;

import br.com.grupo6.apiorder.controllers.OrderController;
import br.com.grupo6.apiorder.dao.OrderDAO;
import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.dto.PutOrderDTO;
import br.com.grupo6.apiorder.models.Order;
import br.com.grupo6.apiorder.util.OrderCreator;
import br.com.grupo6.apiorder.util.PostOrderRequestBody;
import br.com.grupo6.apiorder.util.PutOrderRequestBody;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentMatchers;
import org.mockito.BDDMockito;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@ExtendWith(SpringExtension.class)
@DisplayName("Tests for OrderService")
class OrderServiceImplTest {
    @InjectMocks
    private OrderServiceImpl orderService;

    @Mock
    private OrderDAO orderDAOMock;

    @BeforeEach
    void setUp() {
        PageImpl<Order> orderPage = new PageImpl<>(List.of(OrderCreator.createValidOrder()));
        BDDMockito.when(orderDAOMock.findAll(ArgumentMatchers.any(PageRequest.class)))
                .thenReturn(orderPage);


        BDDMockito.when(orderDAOMock.findById(ArgumentMatchers.any(UUID.class)))
                .thenReturn(Optional.of(OrderCreator.createValidOrder()));

        BDDMockito.when(orderDAOMock.save(ArgumentMatchers.any(Order.class)))
                .thenReturn(OrderCreator.createValidOrder());

        BDDMockito.doNothing().when(orderDAOMock).delete(ArgumentMatchers.any(Order.class));
    }

    @Test
    @DisplayName("Returns page of orders when successful")
    void returnsPageOfOrderswhenSuccessful() {
        Order order = OrderCreator.createValidOrder();

        Page<Order> orderPage = orderService.orderPage(PageRequest.of(1,1));

        Assertions.assertThat(orderPage).isNotNull();
        Assertions.assertThat(orderPage.toList())
                .isNotEmpty()
                .hasSize(1);

        Order orderReturned = orderPage.toList().get(0);

        Assertions.assertThat(orderReturned.getUserId()).isEqualTo(order.getUserId());
        Assertions.assertThat(orderReturned.getOrderDate()).isEqualTo(order.getOrderDate());
        Assertions.assertThat(orderReturned.getDescription()).isEqualTo(order.getDescription());
        Assertions.assertThat(orderReturned.getStatus()).isEqualTo(order.getStatus());
        Assertions.assertThat(orderReturned.getValue()).isEqualTo(order.getValue());
    }


    @Test
    @DisplayName("Returns order found by id")
    void returnsOrderFoundById() {
        Order order = OrderCreator.createValidOrder();

        Order returnedOrder = orderService.findById(order.getId());

        Assertions.assertThat(returnedOrder).isNotNull();
        Assertions.assertThat(returnedOrder.getUserId()).isEqualTo(order.getUserId());
        Assertions.assertThat(returnedOrder.getOrderDate()).isEqualTo(order.getOrderDate());
        Assertions.assertThat(returnedOrder.getDescription()).isEqualTo(order.getDescription());
        Assertions.assertThat(returnedOrder.getStatus()).isEqualTo(order.getStatus());
        Assertions.assertThat(returnedOrder.getValue()).isEqualTo(order.getValue());

    }

    @Test
    @DisplayName("Save order when successful")
    void saveOrderWhenSuccessful() {
        Order order = orderService.save(PostOrderRequestBody.createPostOrderRequestBody());

        Assertions.assertThat(order).isNotNull();
    }

    @Test
    @DisplayName("Update order when successful")
    void updateOrderWhenSuccessful() {
        Order order = OrderCreator.createValidOrder();

        Order returnedOrder = orderService.update(OrderCreator.createValidUpdatedOrder().getId(), PutOrderRequestBody
                        .createPutOrderRequestBodyForUpdate());

        Assertions.assertThat(returnedOrder).isNotNull();
        Assertions.assertThat(returnedOrder.getUserId()).isEqualTo(order.getUserId());
        Assertions.assertThat(returnedOrder.getOrderDate()).isEqualTo(order.getOrderDate());
        Assertions.assertThat(returnedOrder.getDescription()).isEqualTo(order.getDescription());
        Assertions.assertThat(returnedOrder.getStatus()).isEqualTo(order.getStatus());
        Assertions.assertThat(returnedOrder.getValue()).isEqualTo(order.getValue());

    }

    @Test
    @DisplayName("Delete order when successful")
    void deleteOrderWhenSuccessful() {
        Assertions.assertThatCode(() -> orderService.delete(UUID.randomUUID()))
               .doesNotThrowAnyException();

    }

}