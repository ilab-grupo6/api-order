package br.com.grupo6.apiorder.dao;

import br.com.grupo6.apiorder.Util.OrderStatus;
import br.com.grupo6.apiorder.models.Order;
import br.com.grupo6.apiorder.util.OrderCreator;
import org.assertj.core.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.sql.Timestamp;
import java.util.Optional;
import java.util.UUID;

@DataJpaTest
@DisplayName("Tests for OrderDAO")
class OrderDAOTest {
    @Autowired
    private OrderDAO dao;

    @Test
    @DisplayName("Save persist order when succesful")
    void savePersistOrderWhenSuccesful() {
        Order orderToBeSaved = OrderCreator.createOrder();

        Order orderSaved = dao.save(orderToBeSaved);

        Assertions.assertThat(orderSaved).isNotNull();
        Assertions.assertThat(orderSaved.getId()).isNotNull();
        Assertions.assertThat(orderSaved.getUserId()).isEqualTo(orderToBeSaved.getUserId());
        Assertions.assertThat(orderSaved.getOrderDate()).isEqualTo(orderToBeSaved.getOrderDate());
        Assertions.assertThat(orderSaved.getDescription()).isEqualTo(orderToBeSaved.getDescription());
        Assertions.assertThat(orderSaved.getStatus()).isEqualTo(orderToBeSaved.getStatus());
        Assertions.assertThat(orderSaved.getValue()).isEqualTo(orderToBeSaved.getValue());

    }

    @Test
    @DisplayName("Save updates Order when successful")
    void saveUpdatesOrderWhebSuccessful() {
        Order orderToBeSaved = OrderCreator.createOrder();

        Order orderSaved = dao.save(orderToBeSaved);

        orderSaved.setDescription("update test");
        orderSaved.setValue(orderSaved.getValue() + 100);
        orderSaved.setUserId(UUID.randomUUID());
        orderSaved.setStatus(OrderStatus.CONCLUDED);
        orderSaved.setOrderDate(new Timestamp(System.currentTimeMillis()));

        Order orderUpdated = dao.save(orderSaved);

        Assertions.assertThat(orderUpdated).isNotNull();
        Assertions.assertThat(orderUpdated.getId()).isNotNull().isEqualTo(orderSaved.getId());
        Assertions.assertThat(orderUpdated.getUserId()).isEqualTo(orderSaved.getUserId());
        Assertions.assertThat(orderUpdated.getOrderDate()).isEqualTo(orderSaved.getOrderDate());
        Assertions.assertThat(orderUpdated.getDescription()).isEqualTo(orderSaved.getDescription());
        Assertions.assertThat(orderUpdated.getStatus()).isEqualTo(orderSaved.getStatus());
        Assertions.assertThat(orderUpdated.getValue()).isEqualTo(orderSaved.getValue());

    }

    @Test
    @DisplayName("Finds order by id when successful")
    void findsOrderByIdWhenSuccessful() {
        Order orderToBeSaved = OrderCreator.createOrder();

        Order orderSaved = dao.save(orderToBeSaved);

        Optional<Order> optionalOrder = dao.findById(orderSaved.getId());

        Assertions.assertThat(optionalOrder.isPresent()).isTrue();

        Order order = optionalOrder.get();

        Assertions.assertThat(order).isNotNull().isEqualTo(orderSaved);
        Assertions.assertThat(order.getId()).isNotNull().isEqualTo(orderSaved.getId());
        Assertions.assertThat(order.getUserId()).isEqualTo(orderSaved.getUserId());
        Assertions.assertThat(order.getOrderDate()).isEqualTo(orderSaved.getOrderDate());
        Assertions.assertThat(order.getDescription()).isEqualTo(orderSaved.getDescription());
        Assertions.assertThat(order.getStatus()).isEqualTo(orderSaved.getStatus());
        Assertions.assertThat(order.getValue()).isEqualTo(orderSaved.getValue());
    }

    @Test
    @DisplayName("Removes order when successful")
    void removesOrderWhenSuccessful() {
        Order orderToBeSaved = OrderCreator.createOrder();

        Order orderSaved = dao.save(orderToBeSaved);

        dao.delete(orderSaved);

        Optional<Order> orderOptional = dao.findById(orderSaved.getId());

        Assertions.assertThat(orderOptional).isEmpty();
    }


}