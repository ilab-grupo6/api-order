package br.com.grupo6.apiorder.util;

import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.dto.PutOrderDTO;
import br.com.grupo6.apiorder.mapper.OrderMapper;

public class PutOrderRequestBody {
    public static PutOrderDTO createPutOrderRequestBody() {
        return OrderMapper.INSTANCE.toPutOrderDTO(OrderCreator.createOrder());
    }

    public static PutOrderDTO createPutOrderRequestBodyForUpdate() {
        return OrderMapper.INSTANCE.toPutOrderDTO(OrderCreator.createValidUpdatedOrder());
    }
}
