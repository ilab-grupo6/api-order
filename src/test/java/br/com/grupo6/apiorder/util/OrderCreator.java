package br.com.grupo6.apiorder.util;

import br.com.grupo6.apiorder.Util.OrderStatus;
import br.com.grupo6.apiorder.models.Order;

import java.sql.Timestamp;
import java.util.UUID;

public class OrderCreator {
    public static Order createOrder() {
        return Order.builder()
                .userId(UUID.fromString("11671327-16ec-4a3d-a421-9618082c4b93"))
                .description("teste")
                .orderDate(new Timestamp(System.currentTimeMillis()))
                .status(OrderStatus.PENDING)
                .value(50000L)
                .build();
    }

    public static Order createValidOrder() {
        return Order.builder()
                .id(UUID.randomUUID())
                .userId(UUID.fromString("11671327-16ec-4a3d-a421-9618082c4b93"))
                .description("teste")
                .orderDate(new Timestamp(1654786500998L))
                .status(OrderStatus.PENDING)
                .value(50000L)
                .build();
    }

    public static Order createValidUpdatedOrder() {
        return Order.builder()
                .id(UUID.randomUUID())
                .userId(UUID.fromString("11671327-16ec-4a3d-a421-9618082c4b93"))
                .description("teste update")
                .orderDate(new Timestamp(1654786500998L))
                .status(OrderStatus.PENDING)
                .value(50000L)
                .build();
    }
}
