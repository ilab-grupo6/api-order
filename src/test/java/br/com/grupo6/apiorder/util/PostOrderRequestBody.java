package br.com.grupo6.apiorder.util;

import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.mapper.OrderMapper;

public class PostOrderRequestBody {
    public static PostOrderDTO createPostOrderRequestBody() {
        return OrderMapper.INSTANCE.toPostOrderDTO(OrderCreator.createOrder());
    }
}
