package br.com.grupo6.apiorder.services;

import br.com.grupo6.apiorder.models.Order;
import com.google.gson.Gson;
import org.springframework.stereotype.Service;
import software.amazon.awssdk.services.sqs.SqsClient;
import software.amazon.awssdk.services.sqs.model.GetQueueUrlResponse;
import software.amazon.awssdk.services.sqs.model.Message;

import static br.com.grupo6.apiorder.Util.SqsUtil.getQueueUrlResponse;
import static br.com.grupo6.apiorder.Util.SqsUtil.sendMessage;
import static br.com.grupo6.apiorder.Util.UtilObjects.gson;


public class SqsService {
    public static void sendOrder(SqsClient client, Order order, String queue, String aws_account_id) {
        GetQueueUrlResponse result = getQueueUrlResponse(queue, aws_account_id, client);


        sendMessage(client, result.queueUrl(), gson.toJson(order));
    }
}
