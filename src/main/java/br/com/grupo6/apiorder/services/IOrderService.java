package br.com.grupo6.apiorder.services;

import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.dto.PutOrderDTO;
import br.com.grupo6.apiorder.models.Order;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

public interface IOrderService {
    public Order save(PostOrderDTO postOrderDTO);
    public Page<Order> orderPage(Pageable pageable);
    public Order findById(UUID id);
    public Order update(UUID id, PutOrderDTO putOrderDTO);
    public void delete(UUID id);
    public List<Order> findByUserId(UUID id);
}
