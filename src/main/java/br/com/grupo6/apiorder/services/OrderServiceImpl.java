package br.com.grupo6.apiorder.services;


import br.com.grupo6.apiorder.Util.OrderStatus;
import br.com.grupo6.apiorder.dao.OrderDAO;
import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.dto.PutOrderDTO;
import br.com.grupo6.apiorder.exception.BadRequestException;
import br.com.grupo6.apiorder.exception.NotFoundException;
import br.com.grupo6.apiorder.mapper.OrderMapper;
import br.com.grupo6.apiorder.models.Order;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import java.sql.Timestamp;
import java.util.List;
import java.util.UUID;

import static br.com.grupo6.apiorder.Util.SqsUtil.*;
import static br.com.grupo6.apiorder.services.SqsService.sendOrder;


@Service
public class OrderServiceImpl implements IOrderService {

    @Autowired
    private OrderDAO dao;


    @Override
    public Order save(PostOrderDTO postOrderDTO) {
        Order order = OrderMapper.INSTANCE.toOrder(postOrderDTO);
        order.setOrderDate(new Timestamp(System.currentTimeMillis()));
        order.setStatus(OrderStatus.PENDING);
        Order orderSaved = dao.save(order);
        sendOrder(sqsClient,orderSaved, queue, awsAccountId);
        return dao.save(order);
    }

    @Override
    public Page<Order> orderPage(Pageable pageable) {
        return dao.findAll(pageable);
    }

    @Override
    public Order findById(UUID id) {
        return dao.findById(id)
                .orElseThrow(() -> new NotFoundException("Order not Found"));
    }

    @Override
    public Order update(UUID id, PutOrderDTO putOrderDTO) {
        Order savedOrder = findById(id);
        savedOrder.setDescription(putOrderDTO.getDescription());
        savedOrder.setUserId(putOrderDTO.getUserId());
        savedOrder.setValue(putOrderDTO.getValue());

        return dao.save(savedOrder);
    }

    @Override
    public void delete(UUID id) {
        dao.delete(findById(id));
    }

    @Override
    public List<Order> findByUserId(UUID id) {
        return dao.findByUserId(id);
    }

}
