package br.com.grupo6.apiorder.mapper;

import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.dto.PutOrderDTO;
import br.com.grupo6.apiorder.models.Order;
import org.mapstruct.Mapper;
import org.mapstruct.factory.Mappers;

@Mapper(componentModel = "spring")
public abstract class OrderMapper {

    public static final OrderMapper INSTANCE = Mappers.getMapper(OrderMapper.class);

    public abstract Order toOrder(PostOrderDTO postOrderDTO);

    public abstract Order toOrder(PutOrderDTO putOrderDTO);

    public abstract PostOrderDTO toPostOrderDTO(Order order);

    public abstract PutOrderDTO toPutOrderDTO(Order order);
}
