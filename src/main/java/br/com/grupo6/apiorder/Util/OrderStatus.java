package br.com.grupo6.apiorder.Util;

public enum OrderStatus {
    PENDING,
    CONCLUDED,
    FAILURE
}
