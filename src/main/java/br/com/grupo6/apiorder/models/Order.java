package br.com.grupo6.apiorder.models;

import br.com.grupo6.apiorder.Util.OrderStatus;
import lombok.*;
import org.hibernate.Hibernate;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.sql.Timestamp;
import java.util.Objects;
import java.util.UUID;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Entity
@Table(name="orders")
public class Order {
    @Id
    @GeneratedValue(generator = "uuid2")
    @GenericGenerator(name = "uuid2", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", nullable = false)
    private UUID id;
    @Column(name="user_id", nullable=false)
    private UUID userId;
    @Column(name="order_date", nullable = false)
    private Timestamp orderDate;
    @Column(name="status", nullable = false)
    private OrderStatus status;
    @Column(name="order_value", nullable = false)
    private Long value;
    @Column(name="description", length = 250, nullable = false)
    private  String description;

}