package br.com.grupo6.apiorder.dto;

import br.com.grupo6.apiorder.Util.OrderStatus;
import lombok.Builder;
import lombok.Data;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.sql.Timestamp;
import java.util.UUID;

@Data
@Builder
public class PutOrderDTO {

    @NotNull(message = "The order id cannot be null")
    private UUID userId;
    @NotBlank(message = "The order description cannot be empty")
    private String description;
    @NotNull(message = "The order value cannot be null")
    @Min(value = 0, message = "Enter a positive value")
    private Long value;

}