package br.com.grupo6.apiorder.dto;


import lombok.*;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotNull;
import java.util.UUID;


@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class PostOrderDTO {
    @NotNull(message = "The order id cannot be null")
    private UUID userId;
    @NotBlank(message = "The order description cannot be empty")
    private String description;
    @NotNull(message = "The order value cannot be null")
    @Min(value = 0, message = "Enter a positive value")
    private Long value;
}
