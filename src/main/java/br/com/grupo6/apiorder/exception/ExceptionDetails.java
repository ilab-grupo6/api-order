package br.com.grupo6.apiorder.exception;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.SuperBuilder;
import java.sql.Timestamp;

@Data
@SuperBuilder
public class ExceptionDetails {
    protected String title;
    protected int status;
    protected String message;
    @JsonIgnore
    protected String developerMessage;
    protected Timestamp timestamp;
}
