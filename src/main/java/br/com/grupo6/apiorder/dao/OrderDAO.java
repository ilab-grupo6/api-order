package br.com.grupo6.apiorder.dao;

import br.com.grupo6.apiorder.models.Order;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.UUID;

@Repository
public interface OrderDAO extends JpaRepository<Order, UUID> {
    List<Order> findByUserId(UUID id);
}
