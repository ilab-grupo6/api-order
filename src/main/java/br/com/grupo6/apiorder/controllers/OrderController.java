package br.com.grupo6.apiorder.controllers;

import br.com.grupo6.apiorder.Util.OrderStatus;
import br.com.grupo6.apiorder.dao.OrderDAO;
import br.com.grupo6.apiorder.dto.PostOrderDTO;
import br.com.grupo6.apiorder.dto.PutOrderDTO;
import br.com.grupo6.apiorder.models.Order;
import br.com.grupo6.apiorder.services.IOrderService;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.UUID;

@RestController
@RequestMapping("orders")
@CrossOrigin("*")
@RequiredArgsConstructor
@Log4j2
public class OrderController {
    @Autowired
    private IOrderService service;

    @Autowired
    private OrderDAO dao;

    @GetMapping
    public ResponseEntity<Page<Order>> returnPage(Pageable pageable) {
        return ResponseEntity.ok(service.orderPage(pageable));
    }


    @GetMapping(path = "/{id}")
    public ResponseEntity<Order> findById(@PathVariable UUID id) {
        return  ResponseEntity.ok(service.findById(id));
    }

    @GetMapping(path = "/find-by-user-id/{id}")
    public ResponseEntity<List<Order>> findByUserId(@PathVariable UUID id) {
        return ResponseEntity.ok(service.findByUserId(id));
    }

    @PostMapping
    public ResponseEntity<Order> save(@RequestBody @Valid PostOrderDTO postOrderDTO) {
        return new ResponseEntity<>(service.save(postOrderDTO), HttpStatus.CREATED);
    }

    @PutMapping(path = "/{id}")
    public ResponseEntity<Void> update(@RequestBody @Valid PutOrderDTO putOrderDTO, @PathVariable UUID id) {
        service.update(id, putOrderDTO);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    @PutMapping(path = "/set-status/{id}")
    public ResponseEntity<Void> closeOrder(@PathVariable UUID id, @RequestParam(name = "status") OrderStatus status) {
        Order order = service.findById(id);
        order.setStatus(status);
        log.info(order.getStatus());
        dao.save(order);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
    @DeleteMapping(path = "/{id}")
    public ResponseEntity<Void> delete(@PathVariable UUID id) {
        service.delete(id);
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
