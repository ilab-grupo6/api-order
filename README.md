# API ORDER

Esta API depende de uma seguda API para autorizar requisições mediante um token, pois todas as rotas são protegidas e os tokens são verificados em outro serviço.

**A partir daqui, todas os endpoints exigem autenticação.**

#### - GET: /orders?size=5&page=2&sort=value,asc

Este endpoint deve fornecer os dados com uma paginação, onde a página e o tamanho da mesma deve ser informado na URL como mostra o exemplo. Caso não sejam informados, o padrão é retornar a primeira página com o seu tamanho limitado a 10 elementos.

Além disso, os dados podem ser ordenanos de forma crescente ou descrescente de acordo com algum parâmetro informado.

##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
{
	"content": [
		{
			"id": "f4e1f5f3-6305-445d-8cac-8ab0f04b8590",
			"userId": "e1addec5-02be-4c39-ba3c-f4a59be234ac",
			"orderDate": "2022-06-23T14:08:24.575+00:00",
			"status": "CONCLUDED",
			"value": 2499,
			"description": "tomate alemão"
		},
		{
			"id": "ad1d5960-e4e8-4d31-9171-f596c11758ff",
			"userId": "80d5ea81-b097-4268-aa39-980954c8ff07",
			"orderDate": "2022-06-22T18:08:35.280+00:00",
			"status": "PENDING",
			"value": 4000,
			"description": "celular usado"
		},
		{
			"id": "a7595081-3cf9-43ee-9dac-6c90014406f6",
			"userId": "a7f3e4b8-2e08-46c9-b262-74961c41685d",
			"orderDate": "2022-06-23T23:32:45.341+00:00",
			"status": "CONCLUDED",
			"value": 5200,
			"description": "teste"
		},
		{
			"id": "bc9a7a9d-53fd-4bb7-b645-207624585738",
			"userId": "a274e40d-5161-4b63-b3eb-f873f50bf08f",
			"orderDate": "2022-06-23T15:06:56.356+00:00",
			"status": "FAILURE",
			"value": 9900,
			"description": "alface"
		},
		{
			"id": "163e2586-0f73-4d81-9954-6ee770942026",
			"userId": "a7f3e4b8-2e08-46c9-b262-74961c41685d",
			"orderDate": "2022-06-23T00:36:17.625+00:00",
			"status": "CONCLUDED",
			"value": 12000,
			"description": "jogo de talheres"
		}
	],
	"pageable": {
		"sort": {
			"empty": false,
			"sorted": true,
			"unsorted": false
		},
		"offset": 10,
		"pageNumber": 2,
		"pageSize": 5,
		"paged": true,
		"unpaged": false
	},
	"totalPages": 7,
	"totalElements": 31,
	"last": false,
	"size": 5,
	"number": 2,
	"sort": {
		"empty": false,
		"sorted": true,
		"unsorted": false
	},
	"numberOfElements": 5,
	"first": false,
	"empty": false
}
```

#### - GET: /orders/{id}

Este endpoint retorna um pedido através de seu id.

##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
{
	"id": "ad1d5960-e4e8-4d31-9171-f596c11758ff",
	"userId": "80d5ea81-b097-4268-aa39-980954c8ff07",
	"orderDate": "2022-06-22T18:08:35.280+00:00",
	"status": "PENDING",
	"value": 4000,
	"description": "celular usado"
}
```


#### - GET: /orders/find-by-user-id/{id}

Este endpoint retorna um array de pedidos através do userId.

##### Resposta:

###### Em caso de sucesso, a seguinte resposta será obtida (código `200`):

```json
[
	{
		"id": "b577e258-06d7-42d3-bb3f-f940a4eb29cd",
		"userId": "a7f3e4b8-2e08-46c9-b262-74961c41685d",
		"orderDate": "2022-06-22T22:10:51.295+00:00",
		"status": "CONCLUDED",
		"value": 250056,
		"description": "montanha russa de milhoes"
	},
	{
		"id": "9b202b07-fad8-4a2d-b83e-d194e6c3abd3",
		"userId": "a7f3e4b8-2e08-46c9-b262-74961c41685d",
		"orderDate": "2022-06-23T16:44:47.328+00:00",
		"status": "CONCLUDED",
		"value": 4000000,
		"description": "hornet"
	},
	{
		"id": "163e2586-0f73-4d81-9954-6ee770942026",
		"userId": "a7f3e4b8-2e08-46c9-b262-74961c41685d",
		"orderDate": "2022-06-23T00:36:17.625+00:00",
		"status": "CONCLUDED",
		"value": 12000,
		"description": "jogo de talheres"
	},
	{
		"id": "a7595081-3cf9-43ee-9dac-6c90014406f6",
		"userId": "a7f3e4b8-2e08-46c9-b262-74961c41685d",
		"orderDate": "2022-06-23T23:32:45.341+00:00",
		"status": "CONCLUDED",
		"value": 5200,
		"description": "teste"
	}
]
```




#### - POST: /order/

Cadastro de um novo pedido

##### Requisição:


```json
{
    "userId": 1,
    "value": 20000,
    "description": "detalhes do pedido aqui"
}
```

###### Em caso de sucesso, a seguinte resposta será obtida (código `201`):

```json
{
    "id": 1,
    "userId": 1,
    "value": 20000,
    "description": "detalhes do pedido aqui",
    "date": 1648123652533,
    "status": "OPENED"
}
```


#### - PUT: /order/{id}

Atualização de um pedido

##### Requisição:


```json
{
	"userId": "764a5047-1b51-44a8-a18e-3df245c9b041",
	"description": "nova descrição",
	"value": 50000
}
```

###### Em caso de sucesso, a seguinte resposta será obtida: **204 No Content**


#### - DELETE: /order/{id}

Exclusão de um pedido


###### Em caso de sucesso, a seguinte resposta será obtida: **204 No Content**





### OBS:

Todas essas rotas estarão protegidas e necessitam de um token que será validado pela API de admin a cada requisição.
